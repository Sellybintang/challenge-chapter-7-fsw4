import './App.css';

import Navbar from './components/Header/Navbar';
import HeroSection from './components/Header/Hero';
import FAQ from './components/Homepage/FAQ';
import OurService from './components/Homepage/OurService';
import Testimonial from './components/Homepage/Testimonial';
import WhyUs from './components/Homepage/WhyUs';
import Footer from './components/Footer/Footer';

function App() {
  return (
    <div className="App">
      <Navbar/>
      <HeroSection/>
      <FAQ/>
      <OurService/>
      <Testimonial/>
      <WhyUs/>
      <Footer/>
    </div>
  );
}

export default App;
