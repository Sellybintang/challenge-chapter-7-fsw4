import React from "react";
import classes from "./css/Hero.module.css";
import Button from "../UI/Button";

import IconCar from "./img/img_car.png";



const HeroSection=()=>{
    return(
        <header className={classes.hero}>
            <div class="container mt-2">
                    <div class="header-banner">
                    <div class="row align-items-center intro">
                        <div class="col-md-6 intro-text">
                        <h1>Sewa & Rental Mobil Terbaik di <br />kawasan Banyuwangi</h1>
                        <p>
                            Selamat datang di Binar Car Rental. Kami menyediakan mobil
                            kualitas <br />
                            terbaik dengan harga terjangkau. Selalu siap melayani
                            kebutuhanmu <br />untuk sewa mobil selama 24 jam.
                        </p>
                        <button
                            type="button"
                            class="button-global"
                            id="tombol"
                            onClick="search()"
                        >
                            Mulai Sewa Mobil
                        </button>
                        </div>
                        <div class="col-md-6 intro-img">
                        <img class="IconCar"
                            src="./img/img_car.png"
                        />
                        </div>
                    </div>
                    </div>
                </div>

                {/* <!-- End header banner & img banner -->
                <!-- Pop Up Login (Tambahan) --> */}
                <div class="container-fluid">
                    <div class="modal fade" id="modallogin" tabindex="-1">
                    <div class="modal-dialog">
                        <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Please Login</h5>
                            <button
                            type="button"
                            class="btn-close"
                            data-bs-dismiss="modal"
                            aria-label="Close"
                            ></button>
                        </div>
                        <div class="modal-body">
                            <p>Email</p>
                            <input
                            class="form-control"
                            type="email"
                            placeholder="email"
                            name="Email"
                            />
                            <p>Password</p>
                            <input
                            class="form-control"
                            type="password"
                            placeholder="password"
                            name="Password"
                            />
                        </div>
                        <div class="modal-footer">
                            <button
                            type="button"
                            class="btn btn-secondary"
                            data-bs-dismiss="modal"
                            >
                            Register
                            </button>
                            <button type="button" class="btn btn-primary">Login</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    );
};
export default HeroSection;

