import React from "react";
import classes from "./css/Navbar.module.css";
import Logo from "./img/logo.png";


const Navbar = (props)=>{
    return(
        <div className={classes.Navbar} >
            <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="header-navbar-logo" href="#"> <img src = {Logo} alt = "Logo"></img></a>
                <button
                class="navbar-toggler"
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
                >
                <span class="navbar-toggler-icon"></span>
                </button>
                <div
                class="collapse navbar-collapse justify-content-end bg-light"
                id="navbarNav"
                >
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">
                    <a class="nav-link" href="index.html#Services">Our Services</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="index.html#whyus">Why Us</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="index.html#Testimonial">Testimonial</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="index.html#faq">FAQ</a>
                    </li>
                    <li class="nav-item">
                    <button
                        type="button"
                        class="button-global"
                        data-bs-toggle="modal"
                        data-bs-target="#modalregister"
                    >
                        Register
                    </button>
                    </li>
                </ul>
                </div>
                </div>
            </nav>
        </div>
     
    );
};
export default Navbar;