import React from "react";
import classes from "./css/Footer.module.css";

import facebook from "./img/icons/icon_facebook.png";
import instagram from "./img/icons/icon_instagram.png";
import twitter from "./img/icons/icon_twitter.png";
import mail from "./img/icons/icon_mail.png";
import twitch from "./img/icons/icon_twitch.png";
import logo from "./img/logo/Rectangle74.png";





const Footer = ()=>{
    return(
        <div id='footer' className={classes.footer}>
            <footer>
            <div class="container mt-5">
                <div class="row">
                <div class="col-lg-2 col-md-3 p-4">
                    <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
                    <p>binarcarrental@gmail.com</p>
                    <p>081-233-334-808</p>
                </div>
                <div class="ol-lg-2 col-md-3 p-4">
                    <div class="footer-nav">
                    <a href="#Services">
                        <p>Our services</p>
                    </a>
                    <a href="#whyus">
                        <p>Why Us</p>
                    </a>
                    <a href="#Testimonial">
                        <p>Testimonial</p>
                    </a>
                    <a href="#faq">
                        <p>FAQ</p>
                    </a>
                    </div>
                </div>
                <div class="ol-lg-2 col-md-3 p-4">
                    <p>Connect with us</p>
                    <div class="footer-connect-icons">
                    <div class="row">
                        <div class="col-2">
                        <a href="https://www.facebook.com/ariawhan"
                            ><img
                            href="instagram.com/gdariawhan"
                            class="img-fluid"
                            src="./img/icons/icon_facebook.png"
                            alt="facebook"
                        /></a>
                        </div>
                        <div class="col-2">
                        <a href="https://www.instagram.com/gdariawhan"
                            ><img
                            class="img-fluid"
                            src="./img/icons/icon_instagram.png"
                            alt="instagram"
                        /></a>
                        </div>
                        <div class="col-2">
                        <a href="https://twitter.com/gdariawhan"
                            ><img
                            class="img-fluid"
                            src="./img/icons/icon_twitter.png"
                            alt="twitter"
                        /></a>
                        </div>
                        <div class="col-2">
                        <a href="https://gmail.com"
                            ><img
                            class="img-fluid"
                            src="./img/icons/icon_mail.png"
                            alt="mail.png"
                        /></a>
                        </div>
                        <div class="col-2">
                        <a href="https://www.twitch.tv/gdariawhan"
                            ><img
                            class="img-fluid"
                            src="./img/icons/icon_twitch.png"
                            alt="twitch"
                        /></a>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="ol-lg-2 col-md-3 p-4">
                    <p>Copyright Binar 2022</p>
                    <a class="footer-logo" href="#"></a>
                </div>
                </div>
            </div>
            {/* <!-- script bootstrap --> */}
            <script type="text/javascript" src="./vendor/bootstrap/js/bootstrap.min.js"></script>
            {/* <!-- script OwlCarousel2-2.3.4 --> */}
            <script type="text/javascript" src="./vendor/OwlCarousel2-2.3.4//docs/assets/vendors/jquery.min.js"></script>
            <script type="text/javascript" src="./vendor/OwlCarousel2-2.3.4/docs/assets/owlcarousel/owl.carousel.min.js"></script>
            {/* <!-- script owl-carousel --> */}
            <script type="text/javascript" src="./scripts/owlcarousel.js"></script>
            {/* <!-- script Website --> */}
            <script type="text/javascript" src="./scripts/binar.js"></script>
            <script type="text/javascript" src="./scripts/app.js"></script>
            <script type="text/javascript" src="./scripts/cars.js"></script>
            <script type="text/javascript" src="./scripts/main.js"></script>
            </footer>
                
                </div>
    )
}

export default Footer;