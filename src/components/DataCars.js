import React from "react";
import Navbar from "./Header/Navbar";
import HeroSection from "./Header/Hero";
import Footer from "./Footer/Footer";
import SearchCars from "./UI/SearchCars";

const DataCars =()=>{
    return (
        <>
        <Navbar/>
        <HeroSection/>
        <SearchCars/>
        <Footer/>
        </>
    );
};
export default DataCars;