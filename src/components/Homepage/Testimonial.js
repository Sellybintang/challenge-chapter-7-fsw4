import React from "react";


const Testimonial=()=>{
    return(
        <div class="main-testimoni" id="Testimonial">
                <div class="container mt-5">
                    <div class="row">
                        <div class="col-12">
                        <h4>Testimonial</h4>
                        <p>Berbagai review positif dari para pelanggan kami</p>
                        </div>
                    </div>
                </div>
        </div>
    );
};
export default Testimonial;